﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace HaloOnlineTagTool.Common
{
    static class PakUtil
    {
        public static string DatArch(int ArchID)
        {
            switch (ArchID)
            {
                case 0x50000: return "textures.dat";
                case 0x90000: return "textures_b.dat";
                case 0x21000: return "video.dat";
                default: return "resources.dat";
            }
        }

        public static byte[] ExtractFile(string PakName, int fileNum, int sizeFull)
        {
            byte[] filedata = new byte[sizeFull];
            uint bufpos = 0;
            uint c_offset;
            var datreader = new BinaryReader(File.OpenRead(PakName));
            datreader.BaseStream.Position = 0x4;
            datreader.BaseStream.Position = datreader.ReadUInt32();
            datreader.BaseStream.Seek(fileNum * 4, SeekOrigin.Current);
            c_offset = datreader.ReadUInt32();
            datreader.BaseStream.Seek(c_offset, SeekOrigin.Begin);
            Console.WriteLine("Unpacking new file: " + fileNum + " @" + PakName);
            while (bufpos < sizeFull)
            {
                uint c_size;
                uint c_sizex;
                c_size = datreader.ReadUInt32();
                c_sizex = datreader.ReadUInt32();
                Console.WriteLine("Chunk size: " + c_sizex);
                byte[] c_data = new byte[c_sizex];
                c_data = datreader.ReadBytes((int)c_sizex);
                LZ4.LZ4Codec.Decode(c_data, 0, (int)c_sizex, filedata, (int)bufpos, (int)c_size, true);
                bufpos = bufpos + c_size;
            }
            return filedata;
        }
    }
}