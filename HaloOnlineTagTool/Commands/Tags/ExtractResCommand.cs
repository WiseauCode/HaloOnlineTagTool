﻿using System;
using System.Collections.Generic;
using System.IO;
using HaloOnlineTagTool.Serialization;
using HaloOnlineTagTool.TagStructures;
using HaloOnlineTagTool.Common;

namespace HaloOnlineTagTool.Commands.Tags
{
    class ExtractResCommand : Command
    {
        private readonly TagCache _cache;
		private readonly FileInfo _fileInfo;
        private readonly TagDeserializer _deserializer;
       
        private void ExtractTagResource(HaloTag e, string filename, string filter = "")
        {
            byte[] res_data = null;            
            dynamic res_info;
            //string data_arch;
            using (var stream = _fileInfo.OpenRead())  
            switch (e.Class.ToString())
            {
                case "bitm":
                    res_info = Activator.CreateInstance<BitmapTag>();
                    res_info = _deserializer.Deserialize<BitmapTag>(stream, e);
                    break;
                default:
                    return;
            }
            //if (res_info.Height <= 512) return;
            if (filter.Length > 0)
            {
                if (!res_info.Filter(filter)) return;
            }
            if (filename.Length == 0)
            {
                Directory.CreateDirectory(e.Class.ToString());
                filename = string.Format("{0}\\{1:X8}.{2}", e.Class.ToString(), e.Index, res_info.Extension);
            }
            res_data = PakUtil.ExtractFile(Path.Combine(_fileInfo.DirectoryName ?? "", PakUtil.DatArch(res_info.ArchId)), 
                res_info.TOCNum, (int)res_info.Size); 
            using (var outStream = File.Open(filename, FileMode.Create, FileAccess.Write))
            {
                outStream.Write(res_info.Header(), 0, res_info.headerSize);
                outStream.Write(res_data, 0, res_data.Length);
            }
        }

		public ExtractResCommand(TagCache cache, FileInfo fileInfo) : base(
			CommandFlags.Inherit,

			"extractres",
			"Extract tag linked data from resource archives",

            "extractres <tag index> [filename]" + "\n" + 
            "extractres <tag class> [filter string]",
            "For .bitm tags filter string can be:" + "\n" +
            "w:<value> or h:<value> - skip textures which have another than <value> width or height accordingly" + "\n" +
            "w+:<value> or h+:<value> - skip textures which have lower than <value> width or height")
		{
			_cache = cache;
			_fileInfo = fileInfo;
            _deserializer = new TagDeserializer(cache);
		}
        public override bool Execute(List<string> args)
        {
            var extractall = false;
            if (args.Count != 1 && args.Count != 2)
                return false;
            var tag = ArgumentParser.ParseTagIndex(_cache, args[0]);
            if (tag == null)
            {
                if (args[0] == "bitm") extractall = true;
                else
                {
                    Console.Error.WriteLine("Wrong tag index or unsupported tag class.");
                    return false;
                }
            }
            string out_arg = "";
            if (args.Count == 2) out_arg = args[1];           
            if (extractall)
            {
                IEnumerable<HaloTag> classTags;
                classTags = _cache.Tags.FindAllByClass(args[0]);                
                foreach (var tagEntry in classTags)
                {
                    if (tagEntry.ResourceFixups.Count == 1)
                    {
                        Console.WriteLine("Extracting tag linked data for {0:X}.{1}", tagEntry.Index, tagEntry.Class.ToString());
                        ExtractTagResource(tagEntry, "", out_arg);
                    }
                }
                return true;
            }
            ExtractTagResource(tag, out_arg); 
            return true;
        }
    }
}
