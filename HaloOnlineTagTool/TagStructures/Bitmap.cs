﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HaloOnlineTagTool.Serialization;

namespace HaloOnlineTagTool.TagStructures
{
    [TagStructure(Class = "bitm", Size = 0x170)] //size is random estimate
    public class BitmapTag
    {
        [TagElement(Offset = 0x8)] //0x10
        public UInt16 Width { get; set; }

        [TagElement(Offset = 0xA )] //0x12
        public UInt16 Height { get; set; }

        [TagElement(Offset = 0xD)] //0x12
        public byte MipLevels { get; set; }

        [TagElement(Offset = 0x10, Size = 0x4)] //0x6C
        public uint FourCC { get; set; }

        [TagElement(Offset = 0x44)] 
        public int ArchId { get; set; }

        [TagElement(Offset = 0x48)] //0xA4
        public int TOCNum { get; set; }

        [TagElement(Offset = 0x4C)] //0xA8
        public int XSize { get; set; }

        [TagElement(Offset = 0x50)] //0xAC
        public int Size { get; set; }  
  
        public string Extension = "dds";
        public int headerSize = 128;
        public byte[] Header()
        {
            byte[] headtpl = Enumerable.Repeat((byte)0x00, 128).ToArray();
            byte[] headbytes = new byte[] {0x44, 0x44, 0x53, 0x20, 0x7C, 0x00, 0x00, 0x00, 0x07, 0x10, 0x02, 0x00};
            byte[] complexCaps = (BitConverter.GetBytes(0x401008)).ToArray();
            Buffer.BlockCopy(headbytes, 0, headtpl, 0, 12);
            Buffer.BlockCopy((BitConverter.GetBytes(Height)).ToArray(), 0, headtpl, 12, 2);
            Buffer.BlockCopy((BitConverter.GetBytes(Width)).ToArray(), 0, headtpl, 16, 2);
            headtpl[22] = 0x00;
            headtpl[28] = MipLevels;
            headtpl[76] = 0x20;
            headtpl[80] = 0x04;
            if (FourCC >> 8 != 0) Buffer.BlockCopy((BitConverter.GetBytes(FourCC)).ToArray(), 0, headtpl, 84, 4);
            else
            {
                headtpl[80] = 0x41;
                headtpl[88] = 0x20;
                headtpl[93] = 0xFF;
                headtpl[98] = 0xFF;
                headtpl[103] = 0xFF;
                headtpl[104] = 0xFF;
            }
            if (MipLevels > 1) Buffer.BlockCopy(complexCaps, 0, headtpl, 108, 4);
            else
                headtpl[109] = 0x10;
            return headtpl;
        }
        public bool Filter(string f)
        {
            var fcmd = f.Split(':');
            int fval = 0;
            Int32.TryParse(fcmd[1], out fval);
            switch (fcmd[0])
            {
                case "w": if (Width == fval) return true;
                    else return false;
                case "h": if (Height == fval) return true;
                    else return false;
                case "w+": if (Width <= fval) return true;
                    else return false;
                case "h+": if (Height <= fval) return true;
                    else return false;
            }
            return true;
        }
    }
}